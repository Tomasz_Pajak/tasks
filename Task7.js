var map; //Will contain map object.
var marker = false; ////Has the user plotted their location marker? 
        
//Function called to initialize / create the map.
//This is called when the page has loaded.
function initMap() {
 
    //The center location of our map.
    var centerOfMap = new google.maps.LatLng(52.357971, -6.516758);
 
    //Map options.
    var options = {
      center: centerOfMap, //Set center.
      zoom: 7 //The zoom value.
    };
 
    //Create the map object.
    map = new google.maps.Map(document.getElementById('map'), options);
 
    //Listen for any clicks on the map.
    google.maps.event.addListener(map, 'click', function(event) {                
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        //If the marker hasn't been added.
        if(marker === false){
            //Create the marker.
            marker = new google.maps.Marker({
                position: clickedLocation,
                map: map,
                draggable: true //make it draggable
            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function(event){
              getLocal();
            });
        } else{
            //Marker has already been added, so just change its location.
            marker.setPosition(clickedLocation);
        }
        //Get the marker's location.
        getLocal();
    });
}   
    
 function getLocal(){

    var currentLocation = marker.getPosition();

    document.getElementById('new_lat').value = currentLocation.lat(); //latitude
    document.getElementById('new_lng').value = currentLocation.lng(); //longitude

    var lat = currentLocation.lat();
    var lng = currentLocation.lng();

    cityKey = getLocationKey(lat,lng);

    weatherInfo = getWeather(cityKey);

    document.getElementById("local_key").innerHTML = "Location Key: " + cityKey;
    document.getElementById("weather_part_text").innerHTML = " Weather Text: " + weatherInfo['description'];
    document.getElementById("weather_part_tempVal").innerHTML = " Temperature: " + weatherInfo['temp'];

 }  
 
function getLocationKey(lng,lat){

    var city_key = new XMLHttpRequest();
    city_key.open("GET",'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=FPNUkLQBMxwLPZUiQpTlkn8vuXU9bVYe&q='+lng+'%2C'+lat+'&language=en-us&details=true&toplevel=true', false);
    city_key.send(null);
    var r = JSON.parse(city_key.response);
    var CityKey = r.Key;
    
    return CityKey;

}

function getWeather(CityKey){

var weather = new XMLHttpRequest();
weather.open("GET","http://dataservice.accuweather.com/currentconditions/v1/"+CityKey+"?apikey=FPNUkLQBMxwLPZUiQpTlkn8vuXU9bVYe&language=en-us&details=true", false);
weather.send(null);

 var w = JSON.parse(weather.response);
 var description = w[0].WeatherText;
 var temp = w[0].Temperature.Metric.Value;
 var weatherInfo ={
    'description':description,
    'temp':temp
 };

 return weatherInfo;

}
//Load the map when the page has finished loading.
google.maps.event.addDomListener(window, 'load', initMap);