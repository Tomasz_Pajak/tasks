function onSubmit(){

	var lon = document.getElementById("long").value;
	var lat = document.getElementById("lat").value;

	document.getElementById("lon").innerHTML = lon;
	document.getElementById("lan").innerHTML = lat;

	cityKey = getLocationKey(lon,lat);

	weatherInfo = getWeather(cityKey);

	document.getElementById("local_key").innerHTML = "Location Key: " + cityKey;
	document.getElementById("weather_part_text").innerHTML = " Weather Text: " + weatherInfo['description'];
	document.getElementById("weather_part_tempVal").innerHTML = " Temperature: " + weatherInfo['temp'];
}


function getLocationKey(lon,lat){

	var city_key = new XMLHttpRequest();
	city_key.open("GET",'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=FPNUkLQBMxwLPZUiQpTlkn8vuXU9bVYe&q='+lon+'%2C'+lat+'&language=en-us&details=true&toplevel=true', false);
	city_key.send(null);
    var r = JSON.parse(city_key.response);
    var CityKey = r.Key;
	
	return CityKey;

}

function getWeather(CityKey){

var weather = new XMLHttpRequest();
weather.open("GET","http://dataservice.accuweather.com/currentconditions/v1/"+CityKey+"?apikey=FPNUkLQBMxwLPZUiQpTlkn8vuXU9bVYe&language=en-us&details=true", false);
weather.send(null);

 var w = JSON.parse(weather.response);
 var description = w[0].WeatherText;
 var temp = w[0].Temperature.Metric.Value;
 var weatherInfo ={
	'description':description,
 	'temp':temp
 };

 return weatherInfo;

}